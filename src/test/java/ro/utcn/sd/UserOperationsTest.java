package ro.utcn.sd;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ro.utcn.sd.business.*;
import ro.utcn.sd.dao.*;
import ro.utcn.sd.entities.*;
import ro.utcn.sd.output.RequestDto;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class UserOperationsTest {

    private DaoFactory mockDaoFactory;

    private User gherman;
    private User rad;
    private User muntean;
    private User buzea;
    private User norbert;
    private User macavei;

    private Car tesla;
    private Car bmw;
    private Car nissan;

    private ParkingLot parkingLot1;
    private ParkingLot parkingLot2;

    private ParkingSpot testSpot;
    private ParkingSpot testSpot2;

    private Request pendingRequest;
    private Request approvedRequest;

    @After
    public void clearUp() {
        mockDaoFactory = null;

        gherman = null;
        rad = null;
        muntean = null;
        buzea = null;
        norbert = null;
        macavei = null;

        tesla = null;
        bmw = null;
        nissan = null;

        parkingLot1 = null;
        parkingLot2 = null;

        testSpot = null;
        testSpot2 = null;

        pendingRequest = null;
        approvedRequest = null;
    }

    @Before
    public void setUp() {

        //===================USERS===============
        rad = new User();
        rad.setEmail("Rad@gmail.com");
        rad.setPassword("34fsaf");
        rad.setId(0);

        gherman = new User();
        gherman.setEmail("gherman@gmail.com");
        gherman.setPassword("3hmtaf");
        gherman.setId(1);

        muntean = new User();
        muntean.setPassword("3425324");
        muntean.setEmail("muntean@main.ru");
        muntean.setId(2);

        buzea = new User();
        buzea.setPassword("cccvfwe");
        buzea.setEmail("buzea@main.ru");
        buzea.setId(3);

        norbert = new User();
        norbert.setPassword("vvvb");
        norbert.setEmail("norby@main.ru");
        norbert.setId(4);

        macavei = new User();
        macavei.setPassword("vvvb");
        macavei.setEmail("mcv@main.ru");
        macavei.setId(5);

        List<User> testUsers = new ArrayList<>();
        testUsers.add(rad);
        testUsers.add(gherman);
        testUsers.add(muntean);
        testUsers.add(norbert);
        testUsers.add(macavei);
        testUsers.add(buzea);

        //=====================CARS===================
        tesla = new Car();
        tesla.setVim("CJ01TSL");
        tesla.setItpDate(new Date());
        tesla.setUser(rad);
        tesla.setId(0);
        Set<Car> macaveiCars = new HashSet<>();

        bmw = new Car();
        bmw.setVim("CJ01BMW");
        bmw.setItpDate(new Date());
        bmw.setUser(gherman);
        bmw.setId(1);

        nissan = new Car();
        nissan.setVim("CJ01NSN");
        nissan.setItpDate(new Date());
        nissan.setUser(macavei);
        nissan.setId(2);
        macaveiCars.add(nissan);
        macavei.setOwnedCars(macaveiCars);

        List<Car> testCars = new ArrayList<>();
        testCars.add(tesla);
        testCars.add(bmw);
        testCars.add(nissan);

        //=============PARKING LOT===================
        parkingLot1 = new ParkingLot();
        parkingLot1.setAddress("Peana");
        parkingLot1.setCapacity(10);
        parkingLot1.setCurrentCapacity(0);
        parkingLot1.setId(0);

        parkingLot2 = new ParkingLot();
        parkingLot2.setAddress("Primaverii");
        parkingLot2.setCapacity(20);
        parkingLot2.setCurrentCapacity(0);
        parkingLot2.setId(1);

        List<ParkingLot> testLots = new ArrayList<ParkingLot>();
        testLots.add(parkingLot1);
        testLots.add(parkingLot2);

        //=============PARKING SPOTS===================
        testSpot = new ParkingSpot();
        testSpot.setParkingLot(parkingLot1);
        testSpot.setFree(false);
        testSpot.setId(0);
        parkingLot1.getParkingSpots().add(testSpot);

        testSpot2 = new ParkingSpot();
        testSpot2.setParkingLot(parkingLot1);
        testSpot2.setFree(false);
        testSpot2.setId(0);
        parkingLot1.getParkingSpots().add(testSpot);

        List<ParkingSpot> testSpots = new ArrayList<>();
        testSpots.add(testSpot);
        testSpots.add(testSpot2);

        //===============REQUESTS===================
        pendingRequest = new Request();
        pendingRequest.setCreationDate(new Date());
        pendingRequest.setCar(bmw);
        pendingRequest.setStatus(false);
        pendingRequest.setId(0);
        pendingRequest.getParkingLots().add(parkingLot1);

        approvedRequest = new Request();
        approvedRequest.setCreationDate(new Date());
        approvedRequest.setCar(nissan);
        approvedRequest.setStatus(true);
        approvedRequest.setId(1);
        approvedRequest.getParkingLots().add(parkingLot1);
        approvedRequest.getParkingLots().add(parkingLot2);

        List<Request> testRequests = new ArrayList<>();
        testRequests.add(approvedRequest);
        testRequests.add(pendingRequest);

        UserDao mockUserDao = MockUserDao(testUsers);
        CarDao mockCarDao = MockCarDao(testCars);
        RequestDao mockRequestDao = MockRequestDao(testRequests);
        ParkingLotDao mockLotDao = MockParkingLotDao(testLots);
        ParkingSpotDao mockSpotDao = MockParkingSpotDao(testSpots);

        mockDaoFactory = MockDaoFactory(mockUserDao,
                mockCarDao,
                mockRequestDao,
                mockLotDao,
                mockSpotDao);
    }

    @Test
    public void mainTest(){
        testRegisterNewAccount();
        testAddCar();
        testRemoveCar();
        testMakeNewRequest();
        testUpdateRequest();
        testViewRequests();
        testDeleteRequest();
    }

    @Test
    public void testRegisterNewAccount() {
        RegisterAccount subject;
        subject = new RegisterAccount(mockDaoFactory, "example.mail@yahoo.com", "example_pass");
        subject.execute();

        User found = mockDaoFactory.getUserDao().find(6); // 6 is the next id and will be our person's id; TODO
        assertEquals("example.mail@yahoo.com", found.getEmail());
        assertEquals("example_pass", found.getPassword());

        // Make sure we didn't break others
        assertNotNull(mockDaoFactory.getUserDao().find(muntean.getId()));
        assertNotNull(mockDaoFactory.getUserDao().find(buzea.getId()));
    }

    @Test
    public void testAddCar() {
        AddCar subject = new AddCar(mockDaoFactory, 5, new Date(), "CJ01NSN");
        subject.execute();
        Car found = mockDaoFactory.getCarDao().find(2);
        assertEquals("CJ01NSN", found.getVim());
        assertEquals(macavei.getEmail(), found.getUser().getEmail());
        assertEquals(macavei.getId(), found.getUser().getId());

        assertTrue(macavei.getOwnedCars().contains(found));

        assertNotNull(mockDaoFactory.getCarDao().find(nissan.getId()));
    }

    @Test
    public void testRemoveCar() {
        User user = bmw.getUser();

        RemoveCar subject = new RemoveCar(mockDaoFactory, 1);
        subject.execute();

        try {
            mockDaoFactory.getCarDao().find(bmw.getId());
        } catch (NoSuchElementException e) {
        }

        assertFalse(user.getOwnedCars().contains(bmw));


    }

    @Test
    public void testMakeNewRequest() {
        Set<ParkingLot> lots = new HashSet<>();
        lots.add(parkingLot2);
        long[] parkingLotIds = {1};
        MakeRequest subject = new MakeRequest(mockDaoFactory, 0, parkingLotIds);
        subject.execute();

        Request found = mockDaoFactory.getRequestDao().find(2);

        assertEquals(found.getCar().getVim(), tesla.getVim());

        assertEquals(found.getParkingLots().size(), 1);
        assertTrue(found.getParkingLots().contains(parkingLot2));

        assertNull(found.getParkingSpot());
        assertEquals(found.getStatus(), false);

        assertTrue(tesla.getRequests().contains(found));

        assertTrue(parkingLot2.getRequests().contains(found));

        assertNotNull(mockDaoFactory.getRequestDao().find(approvedRequest.getId()));
    }

    @Test
    public void testUpdateRequest() {


        long[] newParkingLots = {0};

        UpdateRequest subject = new UpdateRequest(mockDaoFactory, newParkingLots, 1);
        subject.execute();

        Request found = mockDaoFactory.getRequestDao().find(approvedRequest.getId());

        assertFalse(found.getParkingLots().contains(parkingLot2));

        assertFalse(parkingLot2.getRequests().contains(found));

        assertTrue(pendingRequest.getParkingLots().contains(parkingLot1));
    }

    @Test
    public void testDeleteRequest() {
        Car carToInspect = approvedRequest.getCar();
        Set<ParkingLot> lotsToInspect = approvedRequest.getParkingLots();

        DeleteRequest subject = new DeleteRequest(mockDaoFactory, approvedRequest.getId());
        subject.execute();

        try {
            mockDaoFactory.getRequestDao().find(1);
            fail("The removed request was nonetheless found");
        }
        catch (NoSuchElementException e) {
            // If we got this, all is good
        }

        assertFalse(carToInspect.getRequests().contains(approvedRequest));

        for(ParkingLot lot : lotsToInspect) {
            assertFalse(lot.getRequests().contains(approvedRequest));
        }

        assertNotNull(mockDaoFactory.getRequestDao().find(pendingRequest.getId()));

    }


    @Test
    public void testViewRequests() {
        GetUserRequests subject = new GetUserRequests(mockDaoFactory, macavei.getId());
        List<RequestDto> results = subject.execute();

        assertEquals(results.size(), 1);

    }


    private DaoFactory MockDaoFactory(UserDao mockUserDao, CarDao mockCarDao, RequestDao mockRequestDao, ParkingLotDao mockParkingLotDao, ParkingSpotDao mockParkingSpotDao) {
        return new DaoFactory() {
            @Override
            public UserDao getUserDao() {
                return mockUserDao;
            }

            @Override
            public CarDao getCarDao() {
                return mockCarDao;
            }

            @Override
            public RequestDao getRequestDao() {
                return mockRequestDao;
            }

            @Override
            public ParkingLotDao getParkingLotDao() {
                return mockParkingLotDao;
            }

            @Override
            public ParkingSpotDao getParkingSpotDao() {
                return mockParkingSpotDao;
            }
        };
    }

    private UserDao MockUserDao(List<User> users) {
        return new UserDao() {
            @Override
            public User find(long id) {
                return users.stream().filter(it -> it.getId() == id).findAny().get();
            }

            @Override
            public void insert(User user) {
                users.add(user);

                int index = users.indexOf(user);
                user.setId(index); // Java lists store references, so this affects the list, too
            }

            @Override
            public void update(User user) {
                // Not needed for these tests
            }

            @Override
            public void delete(User user) {
                users.remove(user);
            }
        };
    }

    private CarDao MockCarDao(List<Car> cars) {
        return new CarDao() {
            @Override
            public Car find(long id) {
                return cars.stream().filter(it -> it.getId() == id).findAny().get();
            }

            @Override
            public void insert(Car car) {
                cars.add(car);

                int index = cars.indexOf(car);
                car.setId(index);
            }

            @Override
            public void update(Car car) {
                // Not needed
            }

            @Override
            public void delete(Car car) {
                cars.remove(car);
            }
        };
    }

    private RequestDao MockRequestDao(List<Request> requests) {
        return new RequestDao() {
            @Override
            public Request find(long id) {
                return requests.stream().filter(it -> it.getId() == id).findAny().get();
            }

            @Override
            public void insert(Request request) {
                requests.add(request);

                int index = requests.indexOf(request);
                request.setId(index);
            }

            @Override
            public void update(Request request) {
                for (Request r : requests) { // This is rather inelegant, but does the job of a mock
                    if (r.getId() == request.getId()) {
                        requests.set(requests.indexOf(r), request);
                        break;
                    }
                }
            }

            @Override
            public void delete(Request request) {
                requests.remove(request);
            }

            @Override
            public List<Request> findAll() {
                return requests.stream()
                        .collect(Collectors.toCollection(LinkedList::new));
            }
        };
    }

    private ParkingLotDao MockParkingLotDao(List<ParkingLot> lots) {
        return new ParkingLotDao() {
            @Override
            public ParkingLot find(long id) {
                return lots.stream().filter(it -> it.getId() == id).findAny().get();
            }

            @Override
            public void insert(ParkingLot lot) {
                lots.add(lot);

                int index = lots.indexOf(lot);
                lot.setId(index);
            }

            @Override
            public void update(ParkingLot lot) {
                // Not needed
            }

            @Override
            public void delete(ParkingLot lot) {
                lots.remove(lot);
            }

            @Override
            public List<ParkingLot> findAll() {
                return lots.stream().collect(Collectors.toCollection(LinkedList::new));
            }

            @Override
            public boolean incrementCapacity(ParkingLot toUpdate) {
                return false;
            }
        };
    }

    private ParkingSpotDao MockParkingSpotDao(List<ParkingSpot> spots) {
        return new ParkingSpotDao() {
            @Override
            public ParkingSpot find(long id) {
                return spots.stream().filter(it -> it.getId() == id).findAny().get();
            }

            @Override
            public void insert(ParkingSpot spot) {
                spots.add(spot);

                int index = spots.indexOf(spot);
                spot.setId(index);
            }

            @Override
            public void update(ParkingSpot spot) {
                // Not needed
            }

            @Override
            public void delete(ParkingSpot spot) {
                spots.remove(spot);
            }

            @Override
            public List<ParkingSpot> findAll() {
                return spots.stream().collect(Collectors.toList());
            }

        };
    }

}
