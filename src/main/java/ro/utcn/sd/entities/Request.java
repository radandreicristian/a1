package ro.utcn.sd.entities;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Entity
@Table(name = "request")
public class Request {


    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    @Column(name = "id")
    private long id;

    @Column(name = "creationDate")
    private Date creationDate;

    @Column(name = "status")
    private boolean status;

    @ManyToOne
    @JoinColumn(name="carId", nullable = false)
    private Car car = new Car();


    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "parking_lot_request",
            joinColumns = {@JoinColumn(name = "parkingLotId")},
            inverseJoinColumns = {@JoinColumn(name = "requestId")}
    )
    private Set<ParkingLot> parkingLots = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="parkingSpotId", nullable = true)
    private ParkingSpot parkingSpot;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Set<ParkingLot> getParkingLots() {
        return parkingLots;
    }

    public void setParkingLots(Set<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public ParkingSpot getParkingSpot() {
        return parkingSpot;
    }

    public void setParkingSpot(ParkingSpot parkingSpot) {
        this.parkingSpot = parkingSpot;
    }

    public void remove(ParkingLot parkingLot){
        this.getParkingLots().remove(parkingLot);
    }

    public void add(ParkingLot parkingLot){
        this.getParkingLots().add(parkingLot);
    }
}
