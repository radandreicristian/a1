package ro.utcn.sd.output;

import lombok.Data;

import java.util.Date;

@Data
public class CarDto {

    private String vim;

    private Date itpDate;

    public CarDto(String vim, Date itpDate){
        this.vim = vim;
        this.itpDate = itpDate;
    }

}
