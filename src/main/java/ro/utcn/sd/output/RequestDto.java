package ro.utcn.sd.output;

import lombok.Data;

import java.util.Date;

@Data
public class RequestDto {

    private CarDto carDto;
    private boolean status;
    private Date creationDate;

    public RequestDto(CarDto carDto, boolean status, Date creationDate) {
        this.setCarDto(carDto);
        this.setStatus(status);
        this.setCreationDate(creationDate);
    }


    public CarDto getCarDto() {
        return carDto;
    }

    public void setCarDto(CarDto carDto) {
        this.carDto = carDto;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
