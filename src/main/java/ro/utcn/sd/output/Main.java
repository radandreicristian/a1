package ro.utcn.sd.output;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;
import ro.utcn.sd.business.DeleteRequest;
import ro.utcn.sd.business.MakeRequest;
import ro.utcn.sd.dao.*;
import ro.utcn.sd.dao.impl.jdbc.JdbcCarDao;
import ro.utcn.sd.dao.impl.jdbc.util.ConnectionFactory;
import ro.utcn.sd.entities.Car;
import ro.utcn.sd.entities.ParkingLot;
import ro.utcn.sd.entities.ParkingSpot;
import ro.utcn.sd.entities.User;

import java.io.File;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        ConnectionFactory.createConnection();
        DaoFactory daoFactory = DaoFactory.getInstance(DaoFactory.Type.HIBERNATE);
        UserDao userDao = daoFactory.getUserDao();
        CarDao carDao = daoFactory.getCarDao();
        ParkingLotDao parkingLotDao = daoFactory.getParkingLotDao();
        ParkingSpotDao parkingSpotDao = daoFactory.getParkingSpotDao();
        RequestDao requestDao = daoFactory.getRequestDao();
//
//        User rad = new User();
//        rad.setEmail("rad@gmail.com");
//        rad.setPassword("12345a");
//        rad.setAdmin(true);
//
//        User muntean = new User();
//        muntean.setEmail("mntn@gmail.com");
//        muntean.setPassword("asdf");
//        muntean.setAdmin(true);
//
//        User gherman = new User();
//        gherman.setEmail("ghrmn@gmail.com");
//        gherman.setPassword("bfsfg");
//        gherman.setAdmin(true);
//
//        userDao.insert(rad);
//        userDao.insert(gherman);
//        userDao.insert(muntean);

//        Car mazda = new Car();
//        mazda.setVim("CJ04RAD");
//        mazda.setItpDate(new Date());
//        mazda.setUser(userDao.find(1));
//
//        Car bmw = new Car();
//        bmw.setVim("CJ05RAD");
//        bmw.setItpDate(new Date());
//        bmw.setUser(userDao.find(1));
//
//        Car hyundai = new Car();
//        hyundai.setVim("CJ01GHM");
//        hyundai.setItpDate(new Date());
//        hyundai.setUser(userDao.find(2));
//
//        Car logan = new Car();
//        logan.setVim("CJ01GHM");
//        logan.setItpDate(new Date());
//        logan.setUser(userDao.find(3));
//
//        carDao.insert(mazda);
//        carDao.insert(bmw);
//        carDao.insert(hyundai);
//        carDao.insert(logan);
//
//        ParkingLot parkingLot = new ParkingLot();
//        parkingLot.setAddress("peana");
//        parkingLot.setCapacity(20);
//        parkingLot.setCurrentCapacity(0);
//        parkingLotDao.insert(parkingLot);
//
        ParkingSpot parkingSpot2 = new ParkingSpot();
        parkingSpot2.setFree(true);
       parkingSpot2.setParkingLot(parkingLotDao.find(1));

        parkingSpotDao.insert(parkingSpot2);
        Set<ParkingLot> lots = new HashSet<>();
        lots.add(daoFactory.getParkingLotDao().find(1));
        long[] parkingLotIds = {1};
        MakeRequest subject = new MakeRequest(daoFactory, 1, parkingLotIds);
        subject.execute();

//    }

//
//    public static final String SCRIPT_FILE = "exportScript.sql";
//
//    private static SchemaExport getSchemaExport() {
//
//        SchemaExport export = new SchemaExport();
//        // Script file.
//        File outputFile = new File(SCRIPT_FILE);
//        String outputFilePath = outputFile.getAbsolutePath();
//
//        System.out.println("Export file: " + outputFilePath);
//
//        export.setDelimiter(";");
//        export.setOutputFile(outputFilePath);
//
//        // No Stop if Error
//        export.setHaltOnError(false);
//        //
//        return export;
//    }
//
//    public static void dropDataBase(SchemaExport export, Metadata metadata) {
//        // TargetType.DATABASE - Execute on Databse
//        // TargetType.SCRIPT - Write Script file.
//        // TargetType.STDOUT - Write log to Console.
//        EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE, TargetType.SCRIPT, TargetType.STDOUT);
//
//        export.drop(targetTypes, metadata);
//    }
//
//    public static void createDataBase(SchemaExport export, Metadata metadata) {
//        // TargetType.DATABASE - Execute on Databse
//        // TargetType.SCRIPT - Write Script file.
//        // TargetType.STDOUT - Write log to Console.
//
//        EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE, TargetType.SCRIPT, TargetType.STDOUT);
//
//        SchemaExport.Action action = SchemaExport.Action.CREATE;
//        //
//        export.execute(targetTypes, action, metadata);
//
//        System.out.println("Export OK");
//
//    }
//
//    public static void main(String[] args) {
//
//        // Using Oracle Database.
//
//        String configFileName = "hibernate.cfg.xml";
//
//        // Create the ServiceRegistry from hibernate-xxx.cfg.xml
//        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()//
//                .configure(configFileName).build();
//
//        // Create a metadata sources using the specified service registry.
//        Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();
//
//        SchemaExport export = getSchemaExport();
//
//        System.out.println("Drop Database...");
//        // Drop Database
//        dropDataBase(export, metadata);
//
//        System.out.println("Create Database...");
//        // Create tables
//        createDataBase(export, metadata);
//    }
    }
}