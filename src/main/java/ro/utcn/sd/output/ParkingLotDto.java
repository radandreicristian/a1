package ro.utcn.sd.output;

import lombok.Data;

@Data
public class ParkingLotDto {

    private String address;

    private boolean isFull;

    public ParkingLotDto(String address, boolean isFull){
        this.address = address;
        this.isFull = isFull;
    }
}
