package ro.utcn.sd.dao;

import ro.utcn.sd.entities.ParkingSpot;

import java.util.List;

public interface ParkingSpotDao extends Dao<ParkingSpot> {

    @Override
    ParkingSpot find(long id);

    @Override
    void delete(ParkingSpot toDelete);

    @Override
    void update(ParkingSpot toUpdate);

    @Override
    void insert(ParkingSpot toCreate);

    List<ParkingSpot> findAll();
}
