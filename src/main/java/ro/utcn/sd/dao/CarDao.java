package ro.utcn.sd.dao;

import ro.utcn.sd.entities.Car;

public interface CarDao extends Dao<Car> {

    @Override
    Car find(long id);

    @Override
    void delete(Car toDelete);

    @Override
    void update(Car toUpdate);

    @Override
    void insert(Car toCreate);

}
