package ro.utcn.sd.dao;

import ro.utcn.sd.entities.User;

public interface UserDao extends Dao<User> {

    @Override
    User find(long id);

    @Override
    void delete(User toDelete);

    @Override
    void update(User toUpdate);

    @Override
    void insert(User toCreate);
}
