package ro.utcn.sd.dao;

import ro.utcn.sd.entities.ParkingLot;

import java.util.List;

public interface ParkingLotDao extends Dao<ParkingLot> {

    @Override
    ParkingLot find(long id);

    @Override
    void delete(ParkingLot toDelete);

    @Override
    void update(ParkingLot toUpdate);

    @Override
    void insert(ParkingLot toCreate);

    List<ParkingLot> findAll();

    boolean incrementCapacity(ParkingLot toUpdate);
}
