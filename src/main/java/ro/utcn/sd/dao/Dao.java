package ro.utcn.sd.dao;

public interface Dao<T> {
    T find(long id);

    void delete(T toDelete);

    void update(T toUpdate);

    void insert(T toCreate);
}
