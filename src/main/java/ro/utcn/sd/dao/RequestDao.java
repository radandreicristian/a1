package ro.utcn.sd.dao;

import ro.utcn.sd.entities.Request;

import java.util.List;

public interface RequestDao extends Dao<Request> {


        @Override
        Request find(long id);

        @Override
        void delete(Request toDelete);

        @Override
        void update(Request toUpdate);

        @Override
        void insert(Request toCreate);

        List<Request> findAll();
}
