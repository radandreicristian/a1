package ro.utcn.sd.dao.impl.jdbc;

import ro.utcn.sd.dao.ParkingLotDao;
import ro.utcn.sd.entities.ParkingLot;

import java.util.List;

public class JdbcParkingLotDao implements ParkingLotDao {

    @Override
    public ParkingLot find(long id) {
        return null;
    }

    @Override
    public void delete(ParkingLot toDelete) {

    }

    @Override
    public void update(ParkingLot toUpdate) {

    }

    @Override
    public void insert(ParkingLot toCreate) {

    }

    @Override
    public List<ParkingLot> findAll(){
        return null;
    }

    @Override
    public boolean incrementCapacity(ParkingLot toUpdate){
        return false;
    }
}
