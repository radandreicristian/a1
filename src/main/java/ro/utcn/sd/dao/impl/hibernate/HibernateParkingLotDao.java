package ro.utcn.sd.dao.impl.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ro.utcn.sd.dao.ParkingLotDao;
import ro.utcn.sd.dao.impl.hibernate.util.HibernateUtil;
import ro.utcn.sd.entities.ParkingLot;

import java.util.List;

public class HibernateParkingLotDao implements ParkingLotDao {

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public ParkingLot find(long id){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        ParkingLot parkingLot = currentSession.get(ParkingLot.class, id);
        transaction.commit();
        currentSession.close();
        return parkingLot;
    }

    @Override
    public void delete(ParkingLot toDelete){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.delete(toDelete);
        transaction.commit();
        currentSession.close();
    }

    @Override
    public void update(ParkingLot toUpdate){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.update(toUpdate);
        transaction.commit();
        currentSession.close();
    }

    @Override
    public void insert(ParkingLot toCreate){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.merge(toCreate);
        transaction.commit();
        currentSession.close();
    }

    @Override
    public List<ParkingLot> findAll() {
        Session currentSession = sessionFactory.openSession();
        final String parkingLotQuery = "from ParkingLot";
        Transaction transaction = currentSession.beginTransaction();
        List requests = currentSession.createQuery(parkingLotQuery).list();
        transaction.commit();
        currentSession.close();
        return requests;
    }

    @Override
    public boolean incrementCapacity(ParkingLot toUpdate){
        int newCapacity = toUpdate.getCapacity();
        if(newCapacity >= toUpdate.getCapacity()){
            return false;
        }
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.update(toUpdate);
        transaction.commit();
        currentSession.close();
        return true;
    }


}
