package ro.utcn.sd.dao.impl.jdbc;

import ro.utcn.sd.dao.*;

public class JdbcDaoFactory extends  DaoFactory{
    @Override
    public CarDao getCarDao(){
        return new JdbcCarDao();
    }

    @Override
    public UserDao getUserDao(){
        return new JdbcUserDao();
    }

    @Override
    public RequestDao getRequestDao(){
        return new JdbcRequestDao();
    }

    @Override
    public ParkingLotDao getParkingLotDao(){
        return new JdbcParkingLotDao();
    }

    @Override
    public ParkingSpotDao getParkingSpotDao(){
        return new JdbcParkingSpotDao();
    }
}