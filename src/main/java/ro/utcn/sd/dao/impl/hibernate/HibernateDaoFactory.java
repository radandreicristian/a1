package ro.utcn.sd.dao.impl.hibernate;

import ro.utcn.sd.dao.*;

public class HibernateDaoFactory extends DaoFactory {

    @Override
    public CarDao getCarDao(){
        return new HibernateCarDao();
    }

    @Override
    public  UserDao getUserDao(){
        return new HibernateUserDao();
    }

    @Override
    public RequestDao getRequestDao(){
        return new HibernateRequestDao();
    }

    @Override
    public ParkingLotDao getParkingLotDao(){
        return new HibernateParkingLotDao();
    }

    @Override
    public ParkingSpotDao getParkingSpotDao(){
        return new HibernateParkingSpotDao();
    }

}
