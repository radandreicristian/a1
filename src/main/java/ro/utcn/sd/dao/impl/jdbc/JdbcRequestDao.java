package ro.utcn.sd.dao.impl.jdbc;

import ro.utcn.sd.dao.RequestDao;
import ro.utcn.sd.entities.Request;

import java.util.List;

public class JdbcRequestDao implements RequestDao {
    @Override
    public Request find(long id){
       return null;
    }

    @Override
    public void delete(Request toDelete){

    }

    @Override
    public void update(Request toUpdate){

    }

    @Override
    public void insert(Request toCreate){

    }

    @Override
    public List<Request> findAll(){
       return null;
    }
}
