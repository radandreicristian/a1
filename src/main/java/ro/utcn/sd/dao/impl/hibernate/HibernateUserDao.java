package ro.utcn.sd.dao.impl.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ro.utcn.sd.dao.UserDao;
import ro.utcn.sd.dao.impl.hibernate.util.HibernateUtil;
import ro.utcn.sd.entities.User;


public class HibernateUserDao implements UserDao {

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public User find(long id){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        User user = currentSession.get(User.class, id);
        transaction.commit();
        currentSession.close();
        return user;
    }

    @Override
    public void delete(User toDelete){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.delete(toDelete);
        transaction.commit();
        currentSession.close();
    }

    @Override
    public void update(User toUpdate){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.update(toUpdate);
        transaction.commit();
        currentSession.close();
    }

    @Override

    public void insert(User toCreate){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.merge(toCreate);
        transaction.commit();
        currentSession.close();
    }
}
