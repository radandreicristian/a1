package ro.utcn.sd.dao.impl.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ro.utcn.sd.dao.ParkingSpotDao;
import ro.utcn.sd.dao.impl.hibernate.util.HibernateUtil;
import ro.utcn.sd.entities.ParkingSpot;

import java.util.List;

public class HibernateParkingSpotDao implements ParkingSpotDao {
    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public ParkingSpot find(long id){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        ParkingSpot parkingSpot = currentSession.get(ParkingSpot.class, id);
        transaction.commit();
        currentSession.close();
        return parkingSpot;
    }

    @Override
    public void delete(ParkingSpot toDelete){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.delete(toDelete);
        transaction.commit();
        currentSession.close();
    }

    @Override
    public void update(ParkingSpot toUpdate){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.update(toUpdate);
        transaction.commit();
        currentSession.close();
    }

    @Override
    public void insert(ParkingSpot toCreate){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.merge(toCreate);
        transaction.commit();
        currentSession.close();
    }

    @Override
    public List<ParkingSpot> findAll(){
        Session currentSession = sessionFactory.openSession();
        final String carsQuery = "from ParkingSpot";
        Transaction transaction = currentSession.beginTransaction();
        List parkingSpots = currentSession.createQuery(carsQuery).list();
        transaction.commit();
        currentSession.close();
        return parkingSpots;
    }
}
