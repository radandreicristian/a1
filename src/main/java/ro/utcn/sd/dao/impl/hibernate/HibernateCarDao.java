package ro.utcn.sd.dao.impl.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ro.utcn.sd.dao.CarDao;
import ro.utcn.sd.dao.impl.hibernate.util.HibernateUtil;
import ro.utcn.sd.entities.Car;

public class HibernateCarDao implements CarDao {

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public Car find(long id) {
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        Car car = currentSession.get(Car.class, id);
        transaction.commit();
        currentSession.close();
        return car;
    }

    @Override
    public void delete(Car toDelete) {
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.delete(toDelete);
        transaction.commit();
        currentSession.close();
    }

    @Override
    public void update(Car toUpdate) {
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.update(toUpdate);
        transaction.commit();
        currentSession.close();
    }

    @Override
    public void insert(Car toCreate) {
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.merge(toCreate);
        transaction.commit();
        currentSession.close();
    }
}
