package ro.utcn.sd.dao.impl.jdbc;

import ro.utcn.sd.dao.ParkingSpotDao;
import ro.utcn.sd.entities.ParkingSpot;

import java.util.List;

public class JdbcParkingSpotDao implements ParkingSpotDao {

    @Override
    public ParkingSpot find(long id){
        return null;
    }

    @Override
    public void delete(ParkingSpot toDelete){

    }

    @Override
    public void update(ParkingSpot toUpdate){

    }

    @Override
    public void insert(ParkingSpot toCreate){

    }

    @Override
    public List<ParkingSpot> findAll(){
        return null;
    }

}
