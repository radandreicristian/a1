package ro.utcn.sd.dao.impl.jdbc;

import ro.utcn.sd.dao.CarDao;
import ro.utcn.sd.dao.impl.jdbc.util.ConnectionFactory;
import ro.utcn.sd.entities.Car;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcCarDao extends AbstractJdbcDao<Car> implements CarDao {

    @Override
    public Car find(long id) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement preparedStatement = null;
        String insertString = createSelectQuery("id");
        try {
            preparedStatement = dbConnection.prepareStatement(insertString);
            preparedStatement.setLong(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            //this is unique so we'll only call the methods once, not in a while loop
            if (rs != null) {
                rs.next();
                Car car = new Car();
                car.setId(rs.getLong(1));
                car.setItpDate(rs.getDate(2));
                car.setVim(rs.getString(3));
                return car;
            }
        } catch (SQLException e) {
            System.err.println("Exception in " + this.getClass().getSimpleName());
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
        }
        return null;
    }

    @Override
    public void delete(Car toDelete) {
    }

    @Override
    public void update(Car toUpdate) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement preparedStatement = null;
        String updateString = createUpdateQuery();
        try {
            preparedStatement = dbConnection.prepareStatement(updateString);

            preparedStatement.setString(1, toUpdate.getVim());
            preparedStatement.setDate(2, new java.sql.Date(toUpdate.getItpDate().getTime()));
            preparedStatement.setLong(3, toUpdate.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally{
            ConnectionFactory.close(preparedStatement);
        }
    }

    @Override
    public void insert(Car toCreate) {

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement preparedStatement = null;
        String insertString = createInsertQuery(toCreate);
        try {
            preparedStatement = dbConnection.prepareStatement(insertString);

            preparedStatement.setString(1, null);
            preparedStatement.setString(2, toCreate.getVim());
            preparedStatement.setDate(3, new java.sql.Date(toCreate.getItpDate().getTime()));
            preparedStatement.setLong(4, toCreate.getUser().getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
        }
    }
}
