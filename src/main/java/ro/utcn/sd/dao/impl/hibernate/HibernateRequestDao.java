package ro.utcn.sd.dao.impl.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ro.utcn.sd.dao.RequestDao;
import ro.utcn.sd.dao.impl.hibernate.util.HibernateUtil;
import ro.utcn.sd.entities.Request;

import java.util.List;

public class HibernateRequestDao implements RequestDao {

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public Request find(long id){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        Request request = currentSession.get(Request.class, id);
        transaction.commit();
        currentSession.close();
        return request;
    }

    @Override
    public void delete(Request toDelete){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.delete(toDelete);
        transaction.commit();
        currentSession.close();
    }

    @Override
    public void update(Request toUpdate){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.update(toUpdate);
        transaction.commit();
        currentSession.close();
    }

    @Override
    public void insert(Request toCreate){
        Session currentSession = sessionFactory.openSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.merge(toCreate);
        transaction.commit();
        currentSession.close();
    }

    @Override
    public List<Request> findAll(){
        Session currentSession = sessionFactory.openSession();
        final String carsQuery = "from Request";
        Transaction transaction = currentSession.beginTransaction();
        List requests = currentSession.createQuery(carsQuery).list();
        transaction.commit();
        currentSession.close();
        return requests;
    }
}
