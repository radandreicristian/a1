package ro.utcn.sd.dao.impl.jdbc;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;

public class AbstractJdbcDao<T> {

    private Class<T> type;

    protected AbstractJdbcDao() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public String createSelectAllQuery() {
        StringBuilder sb = new StringBuilder();
        sb
                .append("SELECT ")
                .append("* ")
                .append("FROM `")
                .append(type.getSimpleName())
                .append("`");
        return sb.toString();
    }

    public String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb
                .append("SELECT ")
                .append("* ")
                .append("FROM `")
                .append(type.getSimpleName())
                .append("` WHERE ")
                .append(field)
                .append(" = ?");
        return sb.toString();
    }

    public String createUpdateQuery() {
        StringBuilder sb = new StringBuilder();
        sb
                .append("UPDATE `")
                .append(type.getSimpleName())
                .append("`SET ");
        boolean firstParam = true;
        String prefix = "";
        for (Field field : type.getDeclaredFields()) {
            if (!firstParam) {
                if(field.getType().isPrimitive() || field.getType().getSimpleName().equals("String") || field.getType().getSimpleName().equals("Date")) {
                    sb.append(prefix);
                    prefix = ",";
                    sb.append(field.getName()).append("=?");
                }
            } else {
                firstParam = false;
            }
        }
        sb
                .append(" WHERE ")
                //update an entry based on its id
                .append(type.getDeclaredFields()[0].getName())
                .append("=?");
        return sb.toString();
    }

    public String createDeleteQuery() {
        StringBuilder sb = new StringBuilder();
        sb
                .append("DELETE FROM `")
                .append(type.getSimpleName())
                .append("` WHERE ")
                //this will always be the id.
                .append(type.getDeclaredFields()[0].getName())
                .append("=?");
        return sb.toString();
    }

    public String createInsertQuery(T instance) {
        int noFields = instance.getClass().getDeclaredFields().length;
        StringBuilder sb = new StringBuilder();
        sb
                .append("INSERT INTO `")
                .append(type.getSimpleName())
                .append("` (");
        boolean firstParam = true;
        String prefix = "";
        for (Field field : type.getDeclaredFields()) {
            if (!firstParam) {
                if(field.getType().isPrimitive() || field.getType().getSimpleName().equals("String") || field.getType().getSimpleName().equals("Date")) {
                    sb.append(prefix);
                    prefix = ",";
                    sb.append(field.getName());
                } else{
                    if(field.getName().equals("user")){
                        sb.append(prefix);
                        prefix = ",";
                        sb.append("userId");
                    }
                }
            } else {
                if(field.getType().isPrimitive() || field.getType().getSimpleName().equals("String")){
                    sb.append(field.getName());
                    sb.append(",");
                }
                firstParam = false;
            }
        }
        sb
                .append(")")
                .append(" VALUES ")
                .append(" (");
        prefix = "";
        for (int i = 0; i < noFields - 1; i++) {
            sb.append(prefix);
            prefix = ",";
            sb.append("?");
        }
        sb.append(")");
        return sb.toString();
    }


}
