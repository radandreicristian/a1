package ro.utcn.sd.dao.impl.jdbc;

import ro.utcn.sd.dao.UserDao;
import ro.utcn.sd.dao.impl.jdbc.util.ConnectionFactory;
import ro.utcn.sd.entities.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcUserDao extends AbstractJdbcDao<User> implements UserDao {

    @Override
    public User find(long id) {

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement preparedStatement = null;
        String insertString = createSelectQuery("id");
        try {
            preparedStatement = dbConnection.prepareStatement(insertString);
            preparedStatement.setLong(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            //this is unique so we'll only call the methods once, not in a while loop
            if (rs != null) {
                rs.next();
                User user = new User();
                user.setId(rs.getLong(1));
                user.setAdmin(rs.getBoolean(2));
                user.setEmail(rs.getString(3));
                user.setPassword(rs.getString(4));
                return user;
            }
        } catch (SQLException e) {
            System.err.println("Exception in " + this.getClass().getSimpleName());
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
        }
        return null;
    }

    @Override
    public void delete(User toDelete) {

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement preparedStatement = null;
        String deleteString = createDeleteQuery();
        try {
            preparedStatement = dbConnection.prepareStatement(deleteString);
            preparedStatement.setLong(1, toDelete.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Exception in " + this.getClass().getSimpleName() + ", in " + this.getClass().getEnclosingMethod().getName());
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
        }
    }

    @Override
    public void update(User toUpdate) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement preparedStatement = null;
        String updateString = createUpdateQuery();
        try {
            preparedStatement = dbConnection.prepareStatement(updateString);
            preparedStatement.setString(1, toUpdate.getEmail());
            preparedStatement.setString(2, toUpdate.getPassword());
            preparedStatement.setBoolean(3, toUpdate.isAdmin());
            preparedStatement.setLong(4, toUpdate.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally{
            ConnectionFactory.close(preparedStatement);
        }
    }

    @Override
    public void insert(User toCreate) {

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement preparedStatement = null;
        String insertString = createInsertQuery(toCreate);
        try {
            preparedStatement = dbConnection.prepareStatement(insertString);

            preparedStatement.setString(1, null);
            preparedStatement.setString(2, toCreate.getEmail());
            preparedStatement.setString(3, toCreate.getPassword());
            preparedStatement.setBoolean(4, toCreate.isAdmin());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
        }
    }
}
