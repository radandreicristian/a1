package ro.utcn.sd.business;

import ro.utcn.sd.dao.DaoFactory;
import ro.utcn.sd.entities.Car;
import ro.utcn.sd.entities.ParkingSpot;
import ro.utcn.sd.entities.Request;

import java.util.Set;

public class RemoveCar {

    private final DaoFactory daoFactory;

    private long carId;

    public RemoveCar(DaoFactory daoFactory, long carId) {
        this.daoFactory = daoFactory;
        this.carId = carId;
    }

    public boolean execute() {
        Car car = daoFactory.getCarDao().find(carId);
        if(car != null){
            Set<Request> carRequests = car.getRequests();
            for (Request r : carRequests) {
                if (r.getParkingSpot() != null) {
                    ParkingSpot parkingSpot = r.getParkingSpot();
                    parkingSpot.setFree(true);
                    daoFactory.getParkingSpotDao().update(parkingSpot);
                }
                daoFactory.getRequestDao().delete(r);
            }
            daoFactory.getCarDao().delete(car);
            return true;
        }
        return false;
    }

}
