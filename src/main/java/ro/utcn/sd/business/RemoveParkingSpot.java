package ro.utcn.sd.business;

import ro.utcn.sd.dao.DaoFactory;
import ro.utcn.sd.entities.ParkingLot;
import ro.utcn.sd.entities.ParkingSpot;

public class RemoveParkingSpot {

    private DaoFactory daoFactory;

    private long parkingSpotId;

    public RemoveParkingSpot(DaoFactory daoFactory, long parkingSpotId){
        this.daoFactory = daoFactory;
        this.parkingSpotId = parkingSpotId;
    }

    private boolean execute(){
        ParkingSpot parkingSpot = daoFactory.getParkingSpotDao().find(parkingSpotId);
        if(parkingSpot != null){
            ParkingLot parkingLot = parkingSpot.getParkingLot();
            parkingSpot.setFree(true);
            parkingLot.setCurrentCapacity(parkingLot.getCurrentCapacity()-1);
            daoFactory.getParkingLotDao().update(parkingLot);
            daoFactory.getParkingSpotDao().update(parkingSpot);
            return true;
        }
       return false;
    }
}
