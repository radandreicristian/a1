package ro.utcn.sd.business;

import ro.utcn.sd.dao.DaoFactory;
import ro.utcn.sd.entities.Car;
import ro.utcn.sd.entities.ParkingLot;
import ro.utcn.sd.entities.Request;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class MakeRequest {

    private final DaoFactory daoFactory;

    private long[] parkingLotIds;

    private long carId;

    public MakeRequest(DaoFactory daoFactory, long carId, long[] parkingLotIds) {
        this.daoFactory = daoFactory;
        this.carId = carId;
        this.parkingLotIds = parkingLotIds;
    }

    public void execute() {
        Request request = new Request();
        Car car = daoFactory.getCarDao().find(carId);
        request.setCar(car);
        request.setCreationDate(new Date());
        Set<ParkingLot> parkingLots = new HashSet<>();
        for (int i = 0; i < parkingLotIds.length; i++) {
            ParkingLot parkingLot = daoFactory.getParkingLotDao().find(parkingLotIds[i]);
            parkingLots.add(parkingLot);
            Set<Request> requests = parkingLot.getRequests();
            requests.add(request);
            parkingLot.setRequests(requests);
            daoFactory.getParkingLotDao().update(parkingLot);
        }
        request.setStatus(false);
        request.setParkingLots(parkingLots);
        request.setParkingSpot(null);

        Set<Request> requests = car.getRequests();
        requests.add(request);
        car.setRequests(requests);

        daoFactory.getCarDao().update(car);
        daoFactory.getRequestDao().insert(request);
    }

}
