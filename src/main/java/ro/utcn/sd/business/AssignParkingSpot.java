package ro.utcn.sd.business;

import ro.utcn.sd.dao.DaoFactory;
import ro.utcn.sd.entities.ParkingLot;
import ro.utcn.sd.entities.ParkingSpot;
import ro.utcn.sd.entities.Request;

public class AssignParkingSpot {

    private final DaoFactory daoFactory;

    private long requestId;

    public AssignParkingSpot(DaoFactory daoFactory, long requestId) {
        this.daoFactory = daoFactory;
        this.requestId = requestId;
    }

    public void execute() {

        Request request = daoFactory.getRequestDao().find(requestId);
        ParkingSpot parkingSpot = request
                .getParkingLots().stream()
                .map(this::getFreeParkingSpot)
                .filter(e -> e != null)
                .findFirst()
                .orElse(null);

        if (parkingSpot != null) {
            request.setParkingSpot(parkingSpot);
            request.setStatus(true);
            daoFactory.getRequestDao().update(request);
        }
    }

    private ParkingSpot getFreeParkingSpot(ParkingLot parkingLot) {
        if (daoFactory.getParkingLotDao().incrementCapacity(parkingLot)) {
            ParkingSpot parkingSpot = new ParkingSpot();
            parkingSpot.setFree(false);
            daoFactory.getParkingSpotDao().insert(parkingSpot);
            return parkingSpot;
        } else {
            return null;
        }
    }
}
