package ro.utcn.sd.business;

import ro.utcn.sd.dao.DaoFactory;
import ro.utcn.sd.entities.Request;
import ro.utcn.sd.entities.User;
import ro.utcn.sd.output.CarDto;
import ro.utcn.sd.output.RequestDto;

import java.util.List;
import java.util.stream.Collectors;

public class GetUserRequests {

    private final DaoFactory daoFactory;
    private long userId;

    public GetUserRequests(DaoFactory daoFactory, long userId) {
        this.daoFactory = daoFactory;
        this.userId = userId;
    }

    public List<RequestDto> execute() {
        User user = daoFactory.getUserDao().find(userId);
        List<Request> requestsList = daoFactory.getRequestDao().findAll();
        return requestsList
                .stream()
                .filter(request -> request.getCar().getUser().equals(user))
                .map(GetUserRequests::create)
                .collect(Collectors.toList());
    }

    private static RequestDto create(Request request) {
        return new RequestDto(
                new CarDto(request.getCar().getVim(), request.getCar().getItpDate()),
                request.getStatus(),
                request.getCreationDate());
    }
}
