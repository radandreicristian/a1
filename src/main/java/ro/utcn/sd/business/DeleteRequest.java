package ro.utcn.sd.business;

import ro.utcn.sd.dao.DaoFactory;
import ro.utcn.sd.entities.ParkingLot;
import ro.utcn.sd.entities.Request;

public class DeleteRequest {

    private final DaoFactory daoFactory;
    private long requestId;

    public DeleteRequest(DaoFactory daoFactory, long requestId){
        this.daoFactory = daoFactory;
        this.requestId = requestId;
    }

    public boolean execute(){

        Request request = daoFactory.getRequestDao().find(requestId);
        if(request != null){
            for(ParkingLot parkingLot : request.getParkingLots()){
                parkingLot.getRequests().remove(request);
                daoFactory.getParkingLotDao().update(parkingLot);
            }
                daoFactory.getRequestDao().delete(request);
            return true;
        }
        return false;
    }
}
