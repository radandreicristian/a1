package ro.utcn.sd.business;

import ro.utcn.sd.dao.DaoFactory;
import ro.utcn.sd.entities.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterAccount {

    private final DaoFactory daoFactory;
    private String email;
    private String password;

    public RegisterAccount(DaoFactory daoFactory, String email, String password) {
        this.daoFactory = daoFactory;
        this.email = email;
        this.password = password;
    }

    public boolean execute() {

        if(verifyEmail(email)) {
            User user = new User();
            user.setEmail(email);
            user.setPassword(password);
            user.setAdmin(false);
            daoFactory.getUserDao().insert(user);
            return true;
        }
        return false;
    }

    private boolean verifyEmail(String email){
        Pattern emailPattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = emailPattern.matcher(email);
        return matcher.find();
    }
}
