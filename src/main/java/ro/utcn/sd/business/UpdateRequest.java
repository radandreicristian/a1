package ro.utcn.sd.business;

import ro.utcn.sd.dao.DaoFactory;
import ro.utcn.sd.entities.ParkingLot;
import ro.utcn.sd.entities.Request;

import java.util.HashSet;
import java.util.Set;

public class UpdateRequest {

    private final DaoFactory daoFactory;
    private long requestId;
    private long[] newParkingLotIds;

    public UpdateRequest(DaoFactory daoFactory, long[] newParkingLotIds, long requestId){
        this.daoFactory=daoFactory;
        this.newParkingLotIds =newParkingLotIds;
        this.requestId = requestId;
    }

    public void execute(){
        Request request = daoFactory.getRequestDao().find(requestId);
        Set<ParkingLot> parkingLots = new HashSet<>();
        for(int i = 0; i < newParkingLotIds.length; i++){
            parkingLots.add(daoFactory.getParkingLotDao().find(newParkingLotIds[i]));
        }
        request.setParkingLots(parkingLots);
        daoFactory.getRequestDao().update(request);
    }
}
