package ro.utcn.sd.business;

import ro.utcn.sd.dao.DaoFactory;
import ro.utcn.sd.entities.ParkingLot;
import ro.utcn.sd.output.ParkingLotDto;

import java.util.List;
import java.util.stream.Collectors;

public class GetParkingLots {

    private final DaoFactory daoFactory;

    public GetParkingLots(DaoFactory daoFactory){
        this.daoFactory = daoFactory;
    }

    public List<ParkingLotDto> execute(){
        return daoFactory.getParkingLotDao().findAll()
                .stream()
                .map(GetParkingLots::create)
                .collect(Collectors.toList());
    }

    private static ParkingLotDto create(ParkingLot parkingLot){
        return new ParkingLotDto(parkingLot.getAddress(), parkingLot.getCapacity() == parkingLot.getCapacity());
    }
}
