package ro.utcn.sd.business;

import ro.utcn.sd.dao.DaoFactory;
import ro.utcn.sd.entities.Car;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddCar {

    private final DaoFactory daoFactory;
    private long userId;
    private Date itpDate;
    private String vim;

    public AddCar(DaoFactory daoFactory, long userId, Date itpDate, String vim) {
        this.daoFactory = daoFactory;
        this.userId = userId;
        this.itpDate = itpDate;
        this.vim = vim;
    }

    public boolean execute() {
        if(validateVim(this.vim)) {
            Car car = new Car();
            car.setVim(vim);
            car.setItpDate(itpDate);
            car.setUser(daoFactory.getUserDao().find(userId));
            daoFactory.getCarDao().insert(car);
            return true;
        }
        return false;
    }

    private boolean validateVim(String vim) {
        Pattern emailPattern = Pattern.compile("^[A-Z]{2}+[0-9]{2}+[A-Z]{3}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = emailPattern.matcher(vim);
        return matcher.find();
    }
}
