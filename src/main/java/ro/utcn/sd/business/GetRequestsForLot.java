package ro.utcn.sd.business;

import ro.utcn.sd.dao.DaoFactory;
import ro.utcn.sd.entities.Request;
import ro.utcn.sd.output.CarDto;
import ro.utcn.sd.output.RequestDto;

import java.util.List;
import java.util.stream.Collectors;

public class GetRequestsForLot {

    private final DaoFactory daoFactory;
    private long parkingLotId;

    public GetRequestsForLot(DaoFactory daoFactory, long parkingLotId){
        this.daoFactory = daoFactory;
        this.parkingLotId = parkingLotId;
    }

    public List<RequestDto> execute(){
        return daoFactory
                .getRequestDao()
                .findAll()
                .stream()
                .filter(request -> request.getParkingLots().contains(daoFactory.getParkingLotDao().find(parkingLotId)))
                .map(GetRequestsForLot::create)
                .collect(Collectors.toList());
    }

    public static RequestDto create(Request request){
        return new RequestDto(
                new CarDto(request.getCar().getVim(), request.getCar().getItpDate()),
                request.getStatus(),
                request.getCreationDate());
    }
}
